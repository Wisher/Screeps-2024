import MemoryRole from "./memory.creep";

class CreepsCounter {
    private static instance: CreepsCounter;

    // TODO: make variables per room and functions to total them up
    private static totalCreeps: number = 0;
    private static totalHarvesterCreeps: number = 0;
    private static totalRepairerCreeps: number = 0;
    private static totalBuilderCreeps: number = 0;
    private static totalUpgraderCreeps: number = 0;

    private constructor() {
        for (const creepsKey in Game.creeps) {
            const creep = Game.creeps[creepsKey];
            if (creep.memory.role === MemoryRole.HARVESTER.valueOf()) {
                CreepsCounter.totalHarvesterCreeps++;
            } else if (creep.memory.role === MemoryRole.REPAIRER.valueOf()) {
                CreepsCounter.totalRepairerCreeps++;
            } else if (creep.memory.role === MemoryRole.BUILDER.valueOf()) {
                CreepsCounter.totalBuilderCreeps++;
            } else if (creep.memory.role === MemoryRole.UPGRADER.valueOf()) {
                CreepsCounter.totalUpgraderCreeps++;
            }

            CreepsCounter.totalCreeps++;
        }
    }

    public static getInstance(): CreepsCounter {
        if (!CreepsCounter.instance) {
            CreepsCounter.instance = new CreepsCounter();
        }

        return CreepsCounter.instance;
    }

    public static getTotalCreeps() {
        return CreepsCounter.totalCreeps;
    }

    public static getTotalHarvesterCreeps() {
        return CreepsCounter.totalHarvesterCreeps;
    }

    public static getTotalRepairerCreeps() {
        return CreepsCounter.totalRepairerCreeps;
    }

    public static getTotalBuilderCreeps() {
        return CreepsCounter.totalBuilderCreeps;
    }

    public static getTotalUpgraderCreeps() {
        return CreepsCounter.totalUpgraderCreeps;
    }
}

export { CreepsCounter };
