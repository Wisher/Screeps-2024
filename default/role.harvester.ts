let roleHarvester: {
    /** @param {Creep} creep **/
    run(creep: Creep): any;
    numHarvesters(room: Room): number;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default roleHarvester = {
    run(creep: Creep): any {
        function getStructure(
            id: Id<_HasId> | undefined
        ): StructureExtension | StructureSpawn | StructureTower | StructureStorage | StructureContainer | null {
            if (id === undefined) {
                id = "" as Id<_HasId>;
            }
            const result = Game.getObjectById(id);

            if (result instanceof StructureExtension) {
                return result;
            }
            if (result instanceof StructureSpawn) {
                return result;
            }
            if (result instanceof StructureTower) {
                return result;
            }
            if (result instanceof StructureStorage) {
                return result;
            }
            if (result instanceof StructureContainer) {
                return result;
            }
            return null;
        }

        function moveAndTransfer(
            target: StructureExtension | StructureSpawn | StructureStorage | StructureTower | StructureContainer
        ): void {
            if (creep.transfer(target, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                creep.moveTo(target, { visualizePathStyle: { stroke: "#ffffff" } });
            }
        }

        function findTarget(): Id<_HasId> {
            const targets = creep.room.find(FIND_STRUCTURES, {
                filter: structure => {
                    return (
                        (structure.structureType === STRUCTURE_EXTENSION ||
                            structure.structureType === STRUCTURE_SPAWN ||
                            structure.structureType === STRUCTURE_TOWER ||
                            structure.structureType === STRUCTURE_STORAGE ||
                            structure.structureType === STRUCTURE_CONTAINER) &&
                        structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0
                    );
                }
            });

            const targetsExtentions: AnyStructure[] = [];
            targets.forEach(target => {
                if (target.structureType === STRUCTURE_EXTENSION) {
                    targetsExtentions.push(target);
                }
            });

            const targetSpawns: StructureSpawn[] = [];
            targets.forEach(target => {
                if (target.structureType === STRUCTURE_SPAWN) {
                    targetSpawns.push(target);
                }
            });

            const targetTowers: AnyStructure[] = [];
            targets.forEach(target => {
                if (target.structureType === STRUCTURE_TOWER) {
                    targetTowers.push(target);
                }
            });

            const targetStorages: AnyStructure[] = [];
            targets.forEach(target => {
                if (target.structureType === STRUCTURE_STORAGE) {
                    targetStorages.push(target);
                }
            });

            const targetContainers: AnyStructure[] = [];
            targetContainers.forEach(target => {
                targetContainers.push(target);
            });

            const orderedTargets = [targetsExtentions, targetSpawns, targetTowers, targetStorages, targetContainers];

            let targetIndex = 0;
            let targetId: Id<_HasId> = "" as Id<_HasId>;

            // eslint-disable-next-line @typescript-eslint/prefer-for-of
            for (let i = 0; i < orderedTargets.length; i++) {
                if (orderedTargets[i].length > 0) {
                    targetIndex = Math.floor(Math.random() * orderedTargets[i].length);
                    targetId = orderedTargets[i][targetIndex].id as Id<_HasId>;
                    break;
                }
            }

            return targetId;
        }

        function moveToAndDepositValidTarget() {
            let isValidTarget = false;

            if (creep.memory.target !== ("" as Id<_HasId>)) {
                isValidTarget = true;
            }

            if (isValidTarget) {
                const target = getStructure(creep.memory.target);

                if (target && target.store && target.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
                    moveAndTransfer(target);
                }
            } else {
                creep.moveTo(35, 22); // TODO: refactor to make a named flag and/or random pos on map until a flag is placed
            }
        }

        if (!creep.memory.harvesting && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.harvesting = true;
            creep.say("🔄 harvest");
        }
        if (creep.memory.harvesting && creep.store.getFreeCapacity() === 0) {
            creep.memory.harvesting = false;
            creep.say("🚿 storing");
        }

        if (creep.memory.harvesting) {
            const sources = creep.room.find(FIND_SOURCES);
            if (creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            }
        } else {
            // if target exists, move to it and deposit energy
            if (getStructure(creep.memory.target)) {
                const target = getStructure(creep.memory.target);

                const potentialTarget = getStructure(findTarget());

                // if any structure other than storage is available, deposit to that instead
                if (
                    target &&
                    (target.structureType === STRUCTURE_STORAGE || target.structureType === STRUCTURE_TOWER) &&
                    potentialTarget &&
                    potentialTarget.structureType !== STRUCTURE_STORAGE &&
                    potentialTarget.structureType !== STRUCTURE_TOWER
                ) {
                    if (potentialTarget.store && potentialTarget.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
                        creep.memory.target = potentialTarget.id;

                        moveAndTransfer(target);
                    }
                } else {
                    // if target has space for energy, move to it
                    if (target && target.store && target.store.getFreeCapacity(RESOURCE_ENERGY) > 0) {
                        moveAndTransfer(target);

                        // if no free space, pick a new target, then move to it and deposit
                    } else if (target && target.store && target.store.getFreeCapacity(RESOURCE_ENERGY) === 0) {
                        creep.memory.target = findTarget();

                        moveToAndDepositValidTarget();
                    }
                }

                // if target doesn't exist, add a random target to memory then move to it and deposit
            } else {
                creep.memory.target = findTarget();

                moveToAndDepositValidTarget();
            }
        }
    },
    numHarvesters(room) {
        let numHarvesters: number = 0;
        const sources = room.find(FIND_SOURCES);

        numHarvesters += sources.length * 3;

        return numHarvesters;
    }
};

// module.exports = roleHarvester;
