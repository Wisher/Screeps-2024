import { CreepsCounter } from "./utilities";

import roleBuilder from "./role.builder";
import roleHarvester from "./role.harvester";
import roleRepairer from "./role.repairer";
import roleUpgrader from "./role.upgrader";
import spawnCreep from "./spawn.creep";
import structureTowers from "./structure.towers";
// import * as _ from "lodash";

import MemoryRole from "./memory.creep";

export function loop() {
    for (const name in Memory.creeps) {
        if (!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log("Clearing non-existing creep memory:", name);
        }
    }

    // initialise CreepsCounter after clearing memory
    CreepsCounter.getInstance();

    // const controlledRooms = _.filter(
    //     Game.rooms,
    //     (room: Room) => !!room.controller?.my
    // );

    const myStructureKeys = Object.keys(Game.structures);
    const myStructures: Structure<StructureConstant>[] = myStructureKeys.map(key => Game.structures[key]);

    const spawns: StructureSpawn[] = [];
    const towers: StructureTower[] = [];

    for (const struct of myStructures) {
        if (struct.structureType === STRUCTURE_SPAWN) {
            spawns.push(struct as StructureSpawn);
        }
        if (struct.structureType === STRUCTURE_TOWER) {
            towers.push(struct as StructureTower);
        }
    }

    spawns.forEach(s => {
        spawnCreep.spawn(s.name, s.room);
    });

    structureTowers.run(towers);

    for (const name in Game.creeps) {
        const creep = Game.creeps[name];
        if (creep.memory.role === MemoryRole.HARVESTER.valueOf()) {
            roleHarvester.run(creep);
        }
        if (creep.memory.role === MemoryRole.UPGRADER.valueOf()) {
            roleUpgrader.run(creep);
        }
        if (creep.memory.role === MemoryRole.REPAIRER.valueOf()) {
            roleRepairer.run(creep);
        }
        if (creep.memory.role === MemoryRole.BUILDER.valueOf()) {
            roleBuilder.run(creep);
        }
    }
}
