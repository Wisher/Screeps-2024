import { CreepsCounter } from "./utilities";

let roleUpgrader: {
    /** @param {Creep} creep **/
    run(creep: Creep): any;
    numUpgraders(totalCreeps: number, totalUpgraders: number): number;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default roleUpgrader = {
    /** @param {Creep} creep **/
    run(creep: Creep): any {
        if (creep.memory.upgrading && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.upgrading = false;
            creep.say("🔄 harvest");
        }
        if (!creep.memory.upgrading && creep.store.getFreeCapacity() === 0) {
            creep.memory.upgrading = true;
            creep.say("⚡ upgrade");
        }

        if (creep.memory.upgrading) {
            if (creep.upgradeController(creep.room.controller as StructureController) === ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller as StructureController, {
                    visualizePathStyle: { stroke: "#ffffff" }
                });
            }
        } else {
            const sources = creep.room.find(FIND_SOURCES);
            if (creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            }
        }
    },
    numUpgraders(): number {
        /** Max number of upgraders in a room */
        const maxUpgraders: number = 3; // TODO: base on factors such as number of Sources in a room, cpu usage, tier of existing upgraders etc.
        /** % to adjust maxUpgraders/totalCreeps to, e.g. 7.5 would be 75% */
        const normalizeModifier: number = 7.5;

        /**
         * if totalCreeps - totalUpgraders is <= 7 = maxUpgraders //TODO: make a parameter or calculated result based on number of sources, cpu usage etc.
         * else = (maxUpgraders / totalcreeps) * normalizeModifer >= 1 ? formula : 1
         */
        let result: number = maxUpgraders;

        if (
            CreepsCounter.getTotalCreeps() - CreepsCounter.getTotalUpgraderCreeps() <=
            5 /* TODO: make dependant based on room conditions and cpu usage, e.g. a room with lots of sources/energy should probably have a higher threshold here */
        ) {
            return result;
        } else {
            const adjustedResult: number = Math.floor(
                (maxUpgraders / CreepsCounter.getTotalCreeps()) * normalizeModifier
            );

            if (adjustedResult >= 1) {
                result = adjustedResult;
                return result;
            } else {
                result = 1;
                return result;
            }
        }
    }
};

// module.exports = roleUpgrader;
