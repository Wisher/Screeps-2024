let roleRepairer: {
    run(creep: Creep): void;
    numRepairers(room: Room): number;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default roleRepairer = {
    run(creep): void {
        // get energy
        // refill tower
        // repair roads

        if (!creep.memory.harvesting && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.harvesting = true;
            creep.say("🔄 harvest");
        }
        if (creep.memory.harvesting && creep.store.getFreeCapacity() === 0) {
            creep.memory.harvesting = false;
            creep.say("🔧 repair");
        }
        if (!creep.memory.target || !Game.getObjectById(creep.memory.target)) {
            const towers = creep.room.find(FIND_STRUCTURES, {
                filter: structure => {
                    return structure.structureType === STRUCTURE_TOWER;
                }
            });

            const targetTower = towers[Math.floor(Math.random() * towers.length)];
            creep.memory.target = targetTower.id;
        }

        if (creep.memory.harvesting) {
            const sources = creep.room.find(FIND_SOURCES);
            if (creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            }
        } else {
            const foundStructures = creep.room.find(FIND_STRUCTURES, {
                filter: structure => {
                    return (
                        (structure.hits < structure.hitsMax && structure.structureType !== STRUCTURE_WALL) ||
                        (structure.structureType === STRUCTURE_TOWER &&
                            structure.store.getFreeCapacity(RESOURCE_ENERGY) > 0)
                    );
                }
            });

            const towers: StructureTower[] = [];
            const repairableStructures: AnyStructure[] = [];
            foundStructures.forEach(structure => {
                if (structure.structureType === STRUCTURE_TOWER) {
                    towers.push(structure);
                } else {
                    repairableStructures.push(structure);
                }
            });

            if (towers.length > 0) {
                const tower = towers[0]; // TODO: store in memory so creep doesn't jitter
                if (creep.transfer(tower, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(tower, { visualizePathStyle: { stroke: "#ffffff" } });
                }
            } else if (repairableStructures.length > 0) {
                if (creep.repair(repairableStructures[0]) === ERR_NOT_IN_RANGE) {
                    // TODO: store in memory so creep doesn't jitter
                    creep.moveTo(repairableStructures[0], { visualizePathStyle: { stroke: "#ffffff" } });
                }

                // move creep to waiting position, //TODO: refactor to either be ~~random tower in room, stored in memory~~, if no tower, named flag if it exists then random pos
            } else {
                if (creep.memory.target && Game.getObjectById(creep.memory.target)) {
                    const target = Game.getObjectById(creep.memory.target) as AnyStructure;
                    creep.moveTo(target.pos);
                }
            }
        }
    },
    numRepairers(room): number {
        const maxCreeps: number = 3;
        let calculatedCreeps: number = 0;

        let damagedStucturesCount: number = 0;
        const damagedStructures: AnyStructure[] = [];
        let totalDamage: number = 0;
        let isTowerNeedEnergy = false;
        let neededTowerEnergy: number = 0;
        const minFreeTowerEnergy: number = 80;

        for (const structuresKey in Game.structures) {
            if (Game.structures[structuresKey].room === room) {
                if (Game.structures[structuresKey].hits < Game.structures[structuresKey].hitsMax) {
                    damagedStucturesCount++;
                    damagedStructures.push(Game.structures[structuresKey] as AnyStructure);
                }

                if (Game.structures[structuresKey].structureType === STRUCTURE_TOWER) {
                    const tower = Game.structures[structuresKey] as StructureTower;
                    if (tower.store.getFreeCapacity(RESOURCE_ENERGY) > minFreeTowerEnergy) {
                        isTowerNeedEnergy = true;
                        neededTowerEnergy += tower.store.getFreeCapacity(RESOURCE_ENERGY);
                    }
                }
            }
        }

        damagedStructures.forEach((damagedStucture: AnyStructure) => {
            totalDamage += damagedStucture.hitsMax - damagedStucture.hits;
        });

        // if damaged structures count is high OR total damage is high or tower needs energy, increase repairers
        if (damagedStucturesCount > 0) {
            calculatedCreeps += Math.ceil(damagedStucturesCount / 5);
        }
        if (totalDamage > 0) {
            calculatedCreeps += Math.ceil(totalDamage / 1000);
        }
        if (isTowerNeedEnergy) {
            calculatedCreeps += Math.ceil(neededTowerEnergy / 350) > 1 ? Math.ceil(neededTowerEnergy / 350) : 1;
        }

        return Math.min(maxCreeps, calculatedCreeps);
    }
};
