// eslint-disable-next-line import/no-unresolved
import * as _ from "lodash";
import { creepBodyTier, creepBodyTiers } from "./creep";
import MemoryRole from "./memory.creep";
import roleBuilder from "./role.builder";
import roleHarvester from "./role.harvester";
import roleRepairer from "./role.repairer";
import roleUpgrader from "./role.upgrader";

let spawnCreep: {
    spawn(spawnerName: string, spawnerRoom: Room): void;
};

function creep(namePre: string, spawn: string, role: MemoryRole, body: creepBodyTier): void {
    const newName = namePre + Game.time;
    console.log("Spawning new " + namePre.toLowerCase() + ": " + newName);
    Game.spawns[spawn].spawnCreep(
        body.bodyparts, // TODO: refactor to be a parameter, based on predefined sets chosen based on available energy and maybe some other logic
        newName,
        { memory: { role, partsTier: body.tier } }
    );

    Game.spawns[spawn].store.getCapacity(RESOURCE_ENERGY);
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default spawnCreep = {
    spawn(spawnerName: string, spawnerRoom: Room): void {
        const harvesters = _.filter(Game.creeps, (c: Creep) => c.memory.role === "harvester");
        // console.log('Harvesters: ' + harvesters.length); //TODO: need to add debugging logic to only output some console stuff when debugging is enabled
        const repairers = _.filter(Game.creeps, (c: Creep) => c.memory.role === "repairer");
        // console.log('Repairer: ' + repairers.length);
        const builders = _.filter(Game.creeps, (c: Creep) => c.memory.role === "builder");
        // console.log('Builders: ' + builders.length);
        const upgraders = _.filter(Game.creeps, (c: Creep) => c.memory.role === "upgrader");
        // console.log('Upgrader: ' + upgraders.length);

        // energy available for making creeps, reserves some for max valid harvester level unless energy is too low
        let creepBodyEnergy: number = 0; // TODO: redo to allow for higher tier creeps when harvesters are active at highest tier or something
        // max total room energy minus valid harvester(worker) tier cost OR room.energyAvailable whichever is lower
        let maxValidHarvesterTierIndex = 0;
        creepBodyTiers.worker.forEach(tier => {
            // update the maxHarvesterTier value to the highest possible value
            if (tier.cost <= spawnerRoom.energyCapacityAvailable - creepBodyTiers.worker[0].cost) {
                maxValidHarvesterTierIndex = creepBodyTiers.worker.indexOf(tier);
            }
        });

        const maxAvailableEnergy =
            spawnerRoom.energyCapacityAvailable - creepBodyTiers.worker[maxValidHarvesterTierIndex].cost <=
            spawnerRoom.energyAvailable
                ? spawnerRoom.energyCapacityAvailable - creepBodyTiers.worker[maxValidHarvesterTierIndex].cost
                : spawnerRoom.energyAvailable;

        // available energy for creeps is the max value from, energy capacity available - cost of max harvester tier OR min tier cost
        creepBodyEnergy = Math.max(maxAvailableEnergy, creepBodyTiers.worker[0].cost);

        function creepBody(role: MemoryRole, reduceMaxTierBy?: number): creepBodyTier {
            // TODO: add another parameter to reduce tier if a creep shouldn't be made at the highest available tier
            // make sure reduceMaxTierBy is positive
            if (reduceMaxTierBy) {
                Math.abs(reduceMaxTierBy);
            }

            let bodyTier: creepBodyTier = creepBodyTiers.worker[0]; // energyAvailable will at worst be equal to first tier cost, so this will at worst be set to the first tier
            creepBodyTiers.worker.forEach(tier => {
                if (tier.cost <= creepBodyEnergy) {
                    bodyTier = tier;
                }
            });

            let bodyTierIndex = 0;

            for (let i = 0; i < creepBodyTiers.worker.length; i++) {
                if (creepBodyTiers.worker[i].tier === bodyTier.tier) {
                    bodyTierIndex = i;
                    break;
                }
            }

            if (role === MemoryRole.HARVESTER) {
                return spawnerRoom.energyAvailable >= creepBodyTiers.worker[maxValidHarvesterTierIndex].cost
                    ? creepBodyTiers.worker[maxValidHarvesterTierIndex]
                    : bodyTier;
            } else {
                if (reduceMaxTierBy) {
                    if (!(bodyTierIndex - reduceMaxTierBy >= 0)) {
                        for (let i = reduceMaxTierBy; i >= 0; i--) {
                            if (bodyTierIndex - i >= 0) {
                                reduceMaxTierBy = i;
                                break;
                            }
                        }
                    }
                    console.log("bodyTierIndex: " + (bodyTierIndex - reduceMaxTierBy));

                    return creepBodyTiers.worker[bodyTierIndex - reduceMaxTierBy];
                } else {
                    return bodyTier;
                }
            }
        }

        if (harvesters.length < roleHarvester.numHarvesters(spawnerRoom)) {
            const role = MemoryRole.HARVESTER;
            creep("Harvester", spawnerName, role, creepBody(role));
        } else if (repairers.length < roleRepairer.numRepairers(spawnerRoom)) {
            const role = MemoryRole.REPAIRER;
            creep("Repairer", spawnerName, role, creepBody(role, 2));
        } else if (builders.length < roleBuilder.numBuilders(spawnerRoom)) {
            const role = MemoryRole.BUILDER;
            creep("Builder", spawnerName, role, creepBody(role, 1));
        } else if (upgraders.length < roleUpgrader.numUpgraders()) {
            const role = MemoryRole.UPGRADER;
            creep("Upgrader", spawnerName, role, creepBody(role, 1));
        }

        if (Game.spawns[spawnerName].spawning) {
            const spawningName = Game.spawns[spawnerName].spawning?.name;
            if (spawningName && Game.creeps[spawningName]) {
                const spawningCreep = Game.creeps[spawningName];
                Game.spawns[spawnerName].room.visual.text(
                    "🛠️" + spawningCreep.memory.role,
                    Game.spawns[spawnerName].pos.x + 1,
                    Game.spawns[spawnerName].pos.y,
                    { align: "left", opacity: 0.8 }
                );
            }
        }
    }
};
