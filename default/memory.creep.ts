enum MemoryRole {
    HARVESTER = "harvester",
    REPAIRER = "repairer",
    BUILDER = "builder",
    UPGRADER = "upgrader"
}

export default MemoryRole;
