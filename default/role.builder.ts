let roleBuilder: {
    /** @param {Creep} creep **/
    run(creep: Creep): any;
    numBuilders(room: Room): number;
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default roleBuilder = {
    /** @param {Creep} creep **/
    run(creep: Creep): any {
        if (creep.memory.building && creep.store[RESOURCE_ENERGY] === 0) {
            creep.memory.building = false;
            creep.say("🔄 harvest");
        }
        if (!creep.memory.building && creep.store.getFreeCapacity() === 0) {
            creep.memory.building = true;
            creep.say("🚧 build");
        }

        function findTarget() {
            const targets = creep.room.find(FIND_CONSTRUCTION_SITES);
            if (targets && targets.length > 0) {
                return targets[0];
            }

            return null;
        }

        if (creep.memory.building) {
            if (creep.memory.target) {
                // TODO: test new code, refactor to remove duplicated code
                //  get target
                const target = Game.getObjectById(creep.memory.target);

                // ensure is construction site
                if (target instanceof ConstructionSite) {
                    // moveto & build target
                    if (creep.build(target) === ERR_NOT_IN_RANGE) {
                        creep.moveTo(target, { visualizePathStyle: { stroke: "#ffffff" } });
                    }
                } else {
                    // find new target and moveto/build it
                    const newTarget = findTarget();
                    if (newTarget) {
                        creep.memory.target = newTarget.id;
                        if (creep.build(newTarget) === ERR_NOT_IN_RANGE) {
                            creep.moveTo(newTarget, { visualizePathStyle: { stroke: "#ffffff" } });
                        }
                    } else {
                        creep.moveTo(35, 19);
                    }
                }
            } else {
                const newTarget = findTarget();
                if (newTarget) {
                    creep.memory.target = newTarget.id;
                    if (creep.build(newTarget) === ERR_NOT_IN_RANGE) {
                        creep.moveTo(newTarget, { visualizePathStyle: { stroke: "#ffffff" } });
                    }
                } else {
                    creep.moveTo(35, 19);
                }
            }
        } else {
            const sources = creep.room.find(FIND_SOURCES);
            if (creep.harvest(sources[0]) === ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], { visualizePathStyle: { stroke: "#ffaa00" } });
            }
        }
    },
    numBuilders(room: Room): number {
        const targets = room.find(FIND_CONSTRUCTION_SITES);

        let acumulatedProgressCost = 0;
        targets.forEach(target => {
            acumulatedProgressCost += target.progressTotal - target.progress;
        });

        if (acumulatedProgressCost > 0) {
            return Math.ceil(acumulatedProgressCost / 5000) <= 5 ? Math.ceil(acumulatedProgressCost / 5000) : 5;
        }

        return 0;
    }
};
