interface BodyTier {
    tier: string;
    bodyparts: BodyPartConstant[];
    cost: number;
    weight: number;
}

const PART_COST: {
    [part: string]: number;
} = {
    move: BODYPART_COST.move,
    work: BODYPART_COST.work,
    carry: BODYPART_COST.carry,
    attack: BODYPART_COST.attack,
    // eslint-disable-next-line camelcase
    ranged_attack: BODYPART_COST.ranged_attack,
    heal: BODYPART_COST.heal,
    tough: BODYPART_COST.tough,
    claim: BODYPART_COST.claim
};

const createPartsTier = (tierName: string, bodyparts: BodyPartConstant[], weight: number): BodyTier => {
    bodyparts.forEach(part => {
        if (!Object.prototype.hasOwnProperty.call(PART_COST, part)) {
            throw new Error(`Part ${part} does not exist`);
        }
    });

    return {
        tier: tierName,
        bodyparts,
        cost: bodyparts.reduce((accumulatedCost, part) => accumulatedCost + PART_COST[part], 0),
        weight
    };
};

const partsTiers: { worker: BodyTier[] } = {
    worker: [
        createPartsTier("tier1", [WORK, CARRY, MOVE], 1), // 200
        createPartsTier("tier2", [WORK, WORK, CARRY, MOVE, MOVE], 2), // 350
        createPartsTier("tier3", [WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE], 4), // 500
        createPartsTier("tier4", [WORK, WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE], 6), // 700
        createPartsTier("tier5", [WORK, WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE], 10) // 900
    ]
};

export { partsTiers as creepBodyTiers, BodyTier as creepBodyTier };
