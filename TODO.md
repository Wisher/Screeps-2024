# TODO List

- [x] More Creep Body Tiers
- [ ] adjust spawning to be weight based instead of creep counts (mostly)
- [x] Add weight to tiers
- [x] add logic to reduce tier of non-harvesters
- [ ] make builders remember which structure they were building
  - ensure checks are in place to check
  - [ ] still exists
  - [ ] still needs building
- [ ] adjust harvester spawning logic
  - [ ] more harvesters per source
  - [ ] more harvesters the more energy remaining to harvest
- [ ] make harvesters target a specific random source (each time they collect?)
- [ ] make creeps draw energy from containers/storage when sources empty