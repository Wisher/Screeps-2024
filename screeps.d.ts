declare global {
    interface CreepMemory {
        [name: string]: any,
        role: string;
        partsTier: string;
        building?: boolean;
        harvesting?: boolean;
        target?: Id<_HasId>;
    }

    interface PowerCreepMemory {
        [name: string]: any,
    }

    interface FlagMemory {
        [name: string]: any,
    }

    interface RoomMemory {
        [name: string]: any,
    }

    interface SpawnMemory {
        [name: string]: any,
    }

    interface Memory {
        creeps: {[name: string]: CreepMemory };
        powerCreeps: {[name: string]: PowerCreepMemory };
        flags: {[name: string]: FlagMemory};
        rooms: {[name: string]: RoomMemory};
        spawns: {[name: string]: SpawnMemory};
    }

}

export {};